@extends('page.index')

@section('judul')
    Detail Posts
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Penulis Posts: <b>{{ $posts->users->username }}</b></h3>
        </div>
        <div class="card-body">
            <p>Post dibuat: {{ $posts->created_at }} | Post diupdate: {{ $posts->updated_at }}</p>
            <div class="row">
                <div class="col-md-8" style="padding-right:15px;">
                    <img src="{{ asset('post/' . $posts->foto) }}" class="card-img-top mb-5" alt="posts">
                    <p>{{ $posts->caption }}</p>
                </div>
                <div class="col-md-4" style="padding-left:15px;">
                    <h5>Postingan Lainnya</h5>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <h3>Comment</h3>
            <h5 class="text-uppercase pb-80">{{ $posts->comment->count() }} Comments</h5>

            <form action="/comment" class="my-3" method="POST">
                @csrf
                <div class="form-group">
                    <label>Beri komentar</label>
                    <input type="hidden" value="{{ $posts->id }}" name="posts_id">
                    <textarea name="comment" class="form-control" rows="5"></textarea>
                </div>
                @error('content')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror

                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>


    @forelse ($posts->comment as $comment)
        <div class="card">
            <div class="card-body">
                <div class="container pt-3">
                    <div class="card">
                        <div class="card-body">
                            <b>{{ $comment->user->username }}</b> | <small>{{ $comment->created_at }}</small>
                            <p class="card-text">{{ $comment->comment }}</p>
                        </div>
                    </div>
                </div>
                <h6 class="text-uppercase pb-80">{{ $comment->reply->count() }} Reply</h6>

                @if ($comment->reply->count() > 0)
                    <div class="row">
                        <div class="col-1"></div>
                        <div class="col">
                            @foreach ($comment->reply as $reply)
                                <div class="container pt-3">
                                    <div class="card">
                                        <div class="card-body">
                                            <b>{{ $reply->user->username }}</b> |
                                            <small>{{ $reply->created_at }}</small>
                                            <p class="card-text">{{ $reply->reply }}</p>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                @endforeach
    @endif

    <div class="row">
        <div class="col-1"></div>
        <div class="col">
            <form action="/reply" class="my-3" method="POST">
                @csrf
                <div class="form-group">
                    <label>Reply</label>
                    <input type="hidden" value="{{ $comment->id }}" name="comment_id">
                    <textarea name="reply" class="form-control" rows="2"></textarea>
                </div>
                @error('content')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror

                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
    </div>
    </div>
@empty
    <h4>Tidak ada komentar</h4>
    @endforelse

@endsection
