# Final Project

## Kelompok 7

## Anggota Kelompok

-   Muhammad Azzam Nur Alwi Mansyur
-   Muhammad Hisyam Rayyan Zukhrufar
-   Rifa Fauziyah

## Tema Project

Media Sosial

## ERD

![Media Sosial ERD](public/img/mediasosialerd.png?raw=true "Media Sosial ERD")

## Link Video

https://final-project-7.herokuapp.com/