<?php

namespace App\Http\Controllers;
use App\Posts;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;


class HomeController extends Controller
{
    public function index()
    {
        $posts = Posts::all();

        return view('index', compact('posts'));
    }
}
